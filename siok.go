package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/hashicorp/consul/api"
	"github.com/ogier/pflag"
)

var listenPort uint
var consulAddressAndPort string
var version string

func init() {
	pflag.UintVarP(&listenPort, "port", "p", 31998, "siok listening port")
	pflag.StringVarP(&consulAddressAndPort, "agent", "a", "127.0.0.1:8500", "Consul Agent IP:port")
	pflag.Parse()
}

// getChecks creates an aggregate check output of relevant checks
func getChecks(serviceID string, agent *api.Agent) []*api.AgentCheck {

	var relevantChecks []*api.AgentCheck

	allChecks, err := agent.Checks()
	if err != nil {
		log.Fatalln(err.Error())
	}

	serviceExists := false

	for _, check := range allChecks {

		if check.ServiceID == serviceID {
			serviceExists = true
		}

		// Reminder: if there is no service ID, that means the check/maint is node related
		if (check.ServiceID == serviceID) || (check.ServiceID == "") {
			relevantChecks = append(relevantChecks, check)
		}
	}

	if !serviceExists {
		relevantChecks = append(relevantChecks, &api.AgentCheck{
			Output: "No such service or no check associated with it",
			Status: "critical"})
	}

	return relevantChecks
}

// parseChecks returns the status, in the following order: critical > warning > passing
func parseChecks(checks []*api.AgentCheck) string {
	var passing, warning, critical bool

	for _, check := range checks {
		switch check.Status {
		case "passing":
			passing = true
		case "warning":
			warning = true
		case "critical":
			critical = true
		default:
			passing = true
		}
	}

	switch {
	case critical:
		return "critical"
	case warning:
		return "warning"
	case passing:
		return "passing"
	default:
		return "passing"
	}
}

func getServiceHealth(c *gin.Context) {

	var queryString QueryString
	c.BindQuery(&queryString)
	warnEnabled := parseBoolValue(queryString.Warn)

	var agent *api.Agent
	interfaceAgent, ok := c.Get("agent")
	if !ok {
		log.Fatalln("Couldn't fetch consulAgent from context!")
	}
	agent = interfaceAgent.(*api.Agent)

	checks := getChecks(queryString.ServiceID, agent)

	aggregatedStatus := parseChecks(checks)

	// converting checks to an interface to be used by c.JSON
	var interfaceSlice = make([]interface{}, len(checks))
	for i, check := range checks {
		interfaceSlice[i] = check
	}

	switch aggregatedStatus {
	case "passing":
		c.JSON(200, interfaceSlice)
	case "warning":
		if warnEnabled {
			c.Header("Warning", "Some Consul checks failed, please investigate")
			c.JSON(200, interfaceSlice)
		} else {
			c.JSON(503, interfaceSlice)
		}
	case "critical":
		c.JSON(503, interfaceSlice)
	}
}

func parseBoolValue(val string) bool {
	if val == "true" {
		return true
	}
	return false
}

func consulAgent(address string) gin.HandlerFunc {
	client, err := api.NewClient(&api.Config{Address: address})
	if err != nil {
		log.Fatalln(err.Error())
	}

	return func(c *gin.Context) {
		c.Set("agent", client.Agent())
		c.Next()
	}
}

func main() {
	gin.SetMode(gin.ReleaseMode)
	route := gin.Default()
	route.Use(consulAgent(consulAddressAndPort))
	route.GET("/health", getServiceHealth)
	log.Printf("Running siok (rev %v) on port %v...", version, listenPort)

	err := route.Run(fmt.Sprintf(":%v", listenPort))
	if err != nil {
		log.Printf("siok failed to start: %v", err.Error())
		os.Exit(1)
	}
}

// QueryString defines the data parsed from URL's querystring
type QueryString struct {
	ServiceID string `form:"service"`
	Warn      string `form:"warn"`
}
